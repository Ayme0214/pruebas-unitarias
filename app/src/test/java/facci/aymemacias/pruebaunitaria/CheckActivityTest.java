package facci.aymemacias.pruebaunitaria;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CheckActivityTest {
    @Test
    public void isEmptyEditText_ReturnsTrue(){
    assertTrue(CheckActivity.validateEmpty(""));
    }

    @Test
    public void isEmptyEditText_ReturnsFalse(){
        assertFalse(CheckActivity.validateEmpty("aaaaa"));
    }

    @Test
    public void isUserBD_ReturnTrue() {

        assertTrue(CheckActivity.validateUser("ayme0214@gmail.com","12345678"));
    }

    @Test
    public void isUserBD_ReturnFalse() {

        assertFalse(CheckActivity.validateUser("ayme014@gmail.com","1234567"));
    }

    private void assertFalse(boolean aaaaa) {
    }


    private void assertTrue(boolean validateEmpty) {
    }
}

package facci.aymemacias.pruebaunitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextUsuario, editTextContraseña;
    private Button btnEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();
    }

    private void setViews() {
        btnEnviar = findViewById(R.id.btn_enviar);
        btnEnviar.setOnClickListener(this);
        editTextUsuario = findViewById(R.id.usuario);
        editTextContraseña = findViewById(R.id.contraseña);
    }

    @Override
    public void onClick(View view) {
        if (view == btnEnviar){
            Log.e("Test", editTextUsuario.getText().toString());
            Log.e("Test", editTextContraseña.getText().toString());

            if (validateUse(editTextUsuario.getText().toString(), editTextContraseña.getText().toString())){
                Toast.makeText(this, "Datos correctamente ingresados", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("Test", "Ingrese correctamente los datos");
            }
        }
    }

    private boolean validateUse(String usuario, String contraseña) {
        if (CheckActivity.validateEmpty(usuario) || CheckActivity.validateEmpty(contraseña)) {
            Toast.makeText(this, "No ha ingresado los datos correctamente", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!CheckActivity.validateUser (usuario, contraseña)){
            Toast.makeText(this, "No ha ingresado los datos correctamente", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}